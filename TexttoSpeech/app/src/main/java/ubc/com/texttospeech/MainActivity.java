package ubc.com.texttospeech;

import android.app.Activity;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Locale;


public class MainActivity extends Activity {
    private TextToSpeech tts;
    public static final String MESSAGE = "ca.ubc.texttospeech.MESSAGE";
    public static final String PICK_DATA = "com.sec.android.app.myfiles.PICK_DATA";

    public enum Codes {
        CHOOSE_FILE_REQUEST_CODE;
    }

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.UK);
                }
            }
        });
        tts.setOnUtteranceCompletedListener(new TextToSpeech.OnUtteranceCompletedListener() {
            @Override
            public void onUtteranceCompleted(String utteranceId) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Button button = (Button)findViewById(R.id.playButton);
                        button.setText("Play");
                    }
                });

            }
        });

        setContentView(R.layout.activity_main);
    }

    public String parseFile(String fileName) {
        FileInputStream fileStream = null;
        StringBuilder text = new StringBuilder();
        try {
            fileStream = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            BufferedReader fileReader = new BufferedReader(new InputStreamReader(fileStream));
            String line;
            while ((line = fileReader.readLine()) != null) {
                text.append(line);
                text.append("\n");
            }
            fileStream.close();
        } catch(Exception e) {
            e.printStackTrace();
        }

        return text.toString();
    }

    public void openFile(String minmeType) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(minmeType);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        Intent sIntent = new Intent(PICK_DATA);

        sIntent.putExtra("CONTENT_TYPE", minmeType); sIntent.addCategory(Intent.CATEGORY_DEFAULT);
        Intent chooserIntent;
        if (getPackageManager().resolveActivity(sIntent, 0) != null) {
            chooserIntent = Intent.createChooser(sIntent, "Open file"); chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { intent});
        } else {
            chooserIntent = Intent.createChooser(intent, "Open file");
        }
        try {
            startActivityForResult(chooserIntent, Codes.CHOOSE_FILE_REQUEST_CODE.ordinal());
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), "No suitable File Manager was found.", Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickOpenFile(View view) {
        openFile("text/*");
    }


    public void onClickPlay(View view) {
        Button button = (Button)findViewById(R.id.playButton);

        if(tts.isSpeaking()) {
            tts.stop();
            button.setText("Play");
            return;
        }

        EditText enteredText = (EditText)findViewById(R.id.editText);
        String words = enteredText.getText().toString();

        Spinner spinner = (Spinner) findViewById(R.id.lang);
        Locale locale = Locale.UK;
        switch (spinner.getSelectedItemPosition()) {
            case 1:
                locale = Locale.US;
                break;
            case 2:
                locale = Locale.FRENCH;
                break;
            case 3:
                locale = Locale.GERMAN;
                break;
            default:
                locale = Locale.UK;
                break;
        }
        tts.setLanguage(locale);

        Toast.makeText(getApplicationContext(), words, Toast.LENGTH_SHORT).show();
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,"messageID");
        tts.speak(words, TextToSpeech.QUEUE_FLUSH, map);

        button.setText("Stop");
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == Codes.CHOOSE_FILE_REQUEST_CODE.ordinal()) {
            EditText t = (EditText) findViewById(R.id.editText);

            t.setText(parseFile(data.getDataString().substring(7)));
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
